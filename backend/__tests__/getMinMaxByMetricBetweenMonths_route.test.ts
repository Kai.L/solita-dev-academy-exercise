import supertest from 'supertest';
import app from '../index';
import { MeasurementType } from '../src/types/farmTypes';
const api = supertest(app);

const from_month_year = '8.2020';
const to_month_year = '9.2020';
const farm_id = '61d4037e67d9f0d90ff12d21';
const metric = 'pH';

describe("GET /farms/measurements/min_max_by_metric_between_months", () => {
    it('measurements are returned as json', async () => {
        await api
            .get(`/farms/measurements/min_max_by_metric_between_months?from_month_year=${from_month_year}&to_month_year=${to_month_year}&metric=${metric}&farm_id=${farm_id}`)
            .expect(200)
            .expect('Content-Type', /application\/json/);
    });

    it('data is in correct format', async () => {
        const res = await api
            .get(`/farms/measurements/min_max_by_metric_between_months?from_month_year=${from_month_year}&to_month_year=${to_month_year}&metric=${metric}&farm_id=${farm_id}`);
        const data = res.body as MeasurementType[];

        expect(data[0]).toBeDefined();
        expect(data[1]).toBeDefined();
        expect(data[0]._id).toBeDefined();
        expect(data[0].sensorType).toBeDefined();
        expect(data[0].datetime).toBeDefined();
        expect(data[0].value).toBeDefined();
        expect(data[0].farmId).toBeDefined();
        expect(Object.keys(data[0])).toEqual(['_id', 'sensorType', 'datetime', 'value', 'farmId']);
        expect(data[1]._id).toBeDefined();
        expect(data[1].sensorType).toBeDefined();
        expect(data[1].datetime).toBeDefined();
        expect(data[1].value).toBeDefined();
        expect(data[1].farmId).toBeDefined();
        expect(Object.keys(data[1])).toEqual(['_id', 'sensorType', 'datetime', 'value', 'farmId']);
    });
    it('status is 400 if 1 or all query parameters are missing', async () => {
        await api
            .get(`/farms/measurements/min_max_by_metric_between_months?farm_id=${farm_id}`)
            .expect(400);
        await api
            .get(`/farms/measurements/min_max_by_metric_between_months`)
            .expect(400);
    });
});



afterAll((done) => {
    done();
});