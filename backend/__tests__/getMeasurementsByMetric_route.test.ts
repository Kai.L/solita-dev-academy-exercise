import supertest from 'supertest';
import app from '../index';
import { MeasurementType } from '../src/types/farmTypes';
const api = supertest(app);

const metric = 'rainFall';
const farm_id = '61d4037e67d9f0d90ff12d21';

describe("GET /farms/measurements/by_metric", () => {
    it('measurements are returned as json', async () => {
        await api
            .get(`/farms/measurements/by_metric?metric=${metric}&farm_id=${farm_id}`)
            .expect(200)
            .expect('Content-Type', /application\/json/);
    });

    it('data is in correct format', async () => {
        const res = await api
            .get(`/farms/measurements/by_metric?metric=${metric}&farm_id=${farm_id}`);
        const data = res.body as Array<MeasurementType>;

        data.forEach((measurement: MeasurementType) => {
            expect(measurement._id).toBeDefined();
            expect(measurement.value).toBeDefined();
            expect(measurement.datetime).toBeDefined();
            expect(measurement.sensorType).toBeDefined();
            expect(Object.keys(measurement)).toEqual(['_id', 'sensorType', 'datetime', 'value']);
        });
    });
    it('status is 400 if 1 or all query parameters are missing', async () => {
        await api
            .get(`/farms/measurements/by_metric?farm_id=${farm_id}`)
            .expect(400);
        await api
            .get(`/farms/measurements/by_metric?&farm_id=${farm_id}`)
            .expect(400);
        await api
            .get(`/farms/measurements/by_metric`)
            .expect(400);
    });
});



afterAll((done) => {
    done();
});