import supertest from 'supertest';
import app from '../index';
import { MonthAverageType } from '../src/types/farmTypes';
const api = supertest(app);

const from_month_year = '8.2020';
const to_month_year = '9.2020';
const farm_id = '61d4037e67d9f0d90ff12d21';
const metric = 'rainFall';

describe("GET /farms/measurements/monthly_avg_by_metric_between_months", () => {
    it('measurements are returned as json', async () => {
        await api
            .get(`/farms/measurements/monthly_avg_by_metric_between_months?from_month_year=${from_month_year}&to_month_year=${to_month_year}&metric=${metric}&farm_id=${farm_id}`)
            .expect(200)
            .expect('Content-Type', /application\/json/);
    });

    it('data is in correct format', async () => {
        const res = await api
            .get(`/farms/measurements/monthly_avg_by_metric_between_months?from_month_year=${from_month_year}&to_month_year=${to_month_year}&metric=${metric}&farm_id=${farm_id}`);
        const data = res.body as Array<MonthAverageType>;
        data.forEach((measurement: MonthAverageType) => {
            expect(measurement.value).toBeDefined();
            expect(measurement.datetime).toBeDefined();
            expect(Object.keys(measurement)).toEqual(['value', 'datetime']);
        });
    });
    it('status is 400 if 1 or all query parameters are missing', async () => {
        await api
            .get(`/farms/measurements/monthly_avg_by_metric_between_months?farm_id=${farm_id}`)
            .expect(400);
        await api
            .get(`/farms/measurements/monthly_avg_by_metric_between_months`)
            .expect(400);
    });
});



afterAll((done) => {
    done();
});