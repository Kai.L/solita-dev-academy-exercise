import mongoose from 'mongoose';
import supertest from 'supertest';
import app from '../index';

const api = supertest(app);

describe("GET /farms", () => {
    it('farms are returned as json', async () => {
        await api
            .get('/farms')
            .expect(200)
            .expect('Content-Type', /application\/json/);
    });
    it('data is in correct format', async () => {
        const res = await api
            .get('/farms');
        const data = res.body as Array<{
            name: string,
            _id: mongoose.Types.ObjectId,
        }>;

        data?.forEach((farm: {
            name: string,
            _id: mongoose.Types.ObjectId,
        }) => {
            expect(farm.name).toBeDefined();
            expect(farm._id).toBeDefined();
            expect(Object.keys(farm)).toEqual(['name', '_id']);
        });
    });
});



afterAll(async () => {
    await mongoose.connection.close();
});