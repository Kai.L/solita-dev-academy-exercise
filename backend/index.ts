import express, { Application } from 'express';
import cors from 'cors';
import env from 'dotenv';
import { connectToDatabase } from './src/database/db';
import farmRouter from './src/routes/farmRouter';
import mongoSanitize from 'express-mongo-sanitize';

env.config();
const app: Application = express();

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(mongoSanitize());
app.use('/farms', farmRouter);

const PORT = process.env.PORT;

connectToDatabase()
	.then(() => {
		if (process.env.NODE_ENV !== 'test') {
			app.listen(PORT, () => {
				console.log(`Server is listening on port ${PORT}`);
			});
		}
	})
	.catch((error) => {
		console.log(error);
	});

export default app;
