import express from 'express';
import {
    allFarmsController,
    byMonthController,
    byMetricController,
    byMetricByMonthController,
    monthlyAverageByMetricBetweenMonthsController,
    minMaxByMetricBetweenMonthsController
} from '../controllers/farmController';

const farmRouter = express.Router();

farmRouter.get(
    '/',
    allFarmsController
);

farmRouter.get(
    '/measurements/by_month',
    byMonthController
);

farmRouter.get(
    '/measurements/by_metric',
    byMetricController
);

farmRouter.get(
    '/measurements/by_metric_by_month',
    byMetricByMonthController
);

farmRouter.get(
    '/measurements/monthly_avg_by_metric_between_months',
    monthlyAverageByMetricBetweenMonthsController
);

farmRouter.get(
    '/measurements/min_max_by_metric_between_months',
    minMaxByMetricBetweenMonthsController
);


export default farmRouter;