import mongoose from "mongoose";

export const validateString = (value: string): boolean => {
    if (typeof value === 'string' && value) {
        return true;
    }
    return false;
};
export const validateNumber = (value: number): boolean => {
    if (typeof value === 'number' && value) {
        return true;
    }
    return false;
};
export const validateDate = (value: Date): boolean => {
    try {
        const date = new Date(value);
        date.toISOString();
        return true;
    } catch (e) {
        return false;
    }
};
export const validateId = (value: mongoose.Types.ObjectId): boolean => {
    if (mongoose.Types.ObjectId.isValid(value)) {
        return true;
    }
    return false;
};

export const validatePH = (value: number): boolean => {
    if (typeof value === 'number' && value >= 0 && value <= 14) {
        return true;
    }
    return false;
};

export const validateTemperature = (value: number): boolean => {
    if (typeof value === 'number' && value >= -50 && value <= 100) {
        return true;
    }
    return false;
};

export const validateRainfall = (value: number): boolean => {
    if (typeof value === 'number' && value >= 0 && value <= 500) {
        return true;
    }
    return false;
};

export const validateMeasurement = (sensorType: string, date: Date, value: number): boolean => {
    if (sensorType === 'pH' && validatePH(value) && validateDate(date) && value) {
        return true;
    } else if (sensorType === 'temperature' && validateTemperature(value) && validateDate(date) && value) {
        return true;
    } else if (sensorType === 'rainFall' && validateRainfall(value) && validateDate(date) && value) {
        return true;
    }
    return false;
};

export const validateFarmNameAndId = (name: string, _id: mongoose.Types.ObjectId): boolean => {
    if (validateString(name) && validateId(_id)) {
        return true;
    }
    return false;
};