

export const getISODateStringFromMonthYear = (month_year: string): string => {
    try {
        const monthYearArray = month_year?.split('.');
        const date = new Date();
        date.setUTCDate(1);
        date.setUTCMonth(Number(monthYearArray[0]) - 1);
        date.setUTCFullYear(Number(monthYearArray[1]));
        date.setUTCHours(0);
        date.setUTCMinutes(0);
        date.setUTCSeconds(0);
        const dateAsISO = date.toISOString();
        return dateAsISO;
    } catch (e) {
        return 'Invalid Date';
    }
};