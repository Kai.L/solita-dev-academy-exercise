import mongoose from 'mongoose';
import Measurement from '../../../database/models/measurement';
import { MeasurementType } from '../../../types/farmTypes';
import { validateMeasurement, validateId } from '../validation';

const getMeasurementsByMonth = async (
    farm_id: mongoose.Types.ObjectId,
    fromISODate: Date,
    toISODate: Date
): Promise<MeasurementType[]> => {

    const result = await Measurement.find({
        farmId: farm_id,
        datetime: { $gte: fromISODate, $lte: toISODate }
    }).select('-__v') as MeasurementType[];

    const validatedData = result?.map((measurement) => {
        const sensorType = measurement.sensorType;
        const datetime = measurement.datetime;
        const value = measurement.value;
        const _id = measurement._id;
        return (validateMeasurement(sensorType, datetime, value) && validateId(_id)) ?
            { _id, sensorType, datetime, value } : null;
    }) as MeasurementType[];
    return validatedData;
};

export default getMeasurementsByMonth;