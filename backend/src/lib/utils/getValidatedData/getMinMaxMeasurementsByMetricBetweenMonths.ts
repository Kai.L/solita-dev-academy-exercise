import mongoose from 'mongoose';
import Measurement from '../../../database/models/measurement';
import { MeasurementType } from '../../../types/farmTypes';
import { validateMeasurement } from '../validation';

const getMinMaxMeasurementsByMetricBetweenMonths = async (
    farm_id: mongoose.Types.ObjectId,
    metric: string,
    fromISODate: Date,
    toISODate: Date
): Promise<MeasurementType[]> => {

    const result = await Measurement.find({
        farmId: farm_id,
        datetime: { $gte: fromISODate, $lte: toISODate },
        sensorType: metric
    }).select('-__v') as unknown as MeasurementType[];

    const measurementsSortedByValue = result?.sort((a, b) => a.value - b.value);
    const minMaxMeasurements = {
        min: measurementsSortedByValue[0],
        max: measurementsSortedByValue[measurementsSortedByValue.length - 1]
    };

    const getValidatedMinMaxMeasurements = (minMaxMeasurements: { min: MeasurementType, max: MeasurementType }): MeasurementType[] | [] => {
        const min = minMaxMeasurements.min;
        const max = minMaxMeasurements.max;
        if (validateMeasurement(min?.sensorType, min?.datetime, min?.value) && validateMeasurement(max?.sensorType, max?.datetime, max?.value)) {
            return [min, max];
        }
        return [];
    };
    return getValidatedMinMaxMeasurements(minMaxMeasurements);
};

export default getMinMaxMeasurementsByMetricBetweenMonths;