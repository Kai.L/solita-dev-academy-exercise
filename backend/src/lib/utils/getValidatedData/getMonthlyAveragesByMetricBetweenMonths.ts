import mongoose from 'mongoose';
import Measurement from '../../../database/models/measurement';
import { MeasurementType } from '../../../types/farmTypes';
import { validateNumber, validateString } from '../validation';

const getMeasurementsSortedByDate = (measurements: MeasurementType[]): MeasurementType[] => {
    const measurementsSortedByDate = measurements?.sort((a, b) => {
        const aDate = new Date(a.datetime);
        const bDate = new Date(b.datetime);
        return aDate < bDate ? -1
            : aDate > bDate ? 1
                : 0;
    });
    return measurementsSortedByDate;
};

const getMeasurementsGroupedByMonth = (measurements: MeasurementType[]): MeasurementType[][] => {
    const measurementsSortedByDate = getMeasurementsSortedByDate(measurements);
    const measurementsGroupedByMonth: MeasurementType[][] = [];
    const measurementsOfMonth: MeasurementType[] = [];
    measurementsSortedByDate?.forEach((measurement, i, array) => {
        const date = new Date(measurement.datetime);
        const nextDate = new Date(array[i + 1]?.datetime);
        if (date.getUTCMonth() === nextDate.getUTCMonth() && date.getUTCFullYear() === nextDate.getUTCFullYear()) {
            measurementsOfMonth.push(measurement);
        } else {
            if (date.getUTCMonth() !== nextDate.getUTCMonth() || date.getUTCFullYear() !== nextDate.getUTCFullYear()) {
                measurementsOfMonth.push(measurement);
            }
            measurementsGroupedByMonth.push([...measurementsOfMonth]);
            measurementsOfMonth.length = 0;
        }
    });
    return measurementsGroupedByMonth;
};

const getMonthlyAveragesWithMonthYear = (measurements: MeasurementType[]) => {
    const measurementsGroupedByMonth = getMeasurementsGroupedByMonth(measurements);
    const monthlyAveragesWithMonthYear = measurementsGroupedByMonth?.map((measurements) => {
        const sum = measurements.reduce((a, b) => a + b.value, 0);
        const date = new Date(measurements[0]?.datetime);
        const averageWithMonthYear = {
            average: sum / measurements.length,
            monthYear: `${date.getUTCMonth() + 1}.${date.getUTCFullYear()}`
        };
        return averageWithMonthYear;
    });
    return monthlyAveragesWithMonthYear;
};

const getMonthlyAveragesByMetricBetweenMonths = async (
    farm_id: mongoose.Types.ObjectId,
    metric: string,
    fromISODate: Date,
    toISODate: Date
): Promise<MeasurementType[]> => {

    const result = await Measurement.find({
        farmId: farm_id,
        datetime: { $gte: fromISODate, $lte: toISODate },
        sensorType: metric
    }).select('-__v') as unknown as MeasurementType[];
    
    const monthlyAveragesWithMonthYear = getMonthlyAveragesWithMonthYear(result);

    const validatedData = monthlyAveragesWithMonthYear?.map((avgWithMonthYear) => {
        const average = avgWithMonthYear.average;
        const monthYear = avgWithMonthYear.monthYear;
        return (validateNumber(average) && validateString(monthYear)) ?
            { value: average, datetime: monthYear } : null;
    }) as unknown as MeasurementType[];
    return validatedData;
};

export default getMonthlyAveragesByMetricBetweenMonths;