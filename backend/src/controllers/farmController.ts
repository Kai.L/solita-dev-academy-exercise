import Farm from '../database/models/farm';
import { getISODateStringFromMonthYear } from '../lib/utils/dates';
import { Request, Response } from 'express';
import { validateDate, validateFarmNameAndId, validateId, validateString } from '../lib/utils/validation';
import mongoose from 'mongoose';
import getMeasurementsByMonth from '../lib/utils/getValidatedData/getMeasurementsByMonth';
import getMeasurementsByMetric from '../lib/utils/getValidatedData/getMeasurementsByMetric';
import getMeasurementsByMetricByMonth from '../lib/utils/getValidatedData/getMeasurementsByMetricByMonth';
import getMonthlyAveragesByMetricBetweenMonths from '../lib/utils/getValidatedData/getMonthlyAveragesByMetricBetweenMonths';
import getMinMaxMeasurementsByMetricBetweenMonths from '../lib/utils/getValidatedData/getMinMaxMeasurementsByMetricBetweenMonths';

export const allFarmsController = (
    _req: Request, res: Response
): void => {
    Farm.find({}, { name: 1 }).select('-__v')
        .then((result) => {
            const validatedData = result?.map((farm) => {
                const name = farm.name as string;
                const _id = farm._id as mongoose.Types.ObjectId;
                return validateFarmNameAndId(name, _id) ?
                    { name, _id } : null;
            });
            res.status(200).json(validatedData);
        })
        .catch(() => {
            res.status(500).send('Internal server error');
        });
};

export const byMonthController = (
    req: Request, res: Response
): void => {
    const { month_year } = req.query as { [key: string]: string };
    const { farm_id } = req.query as unknown as { [key: string]: mongoose.Types.ObjectId };
    const fromISODate = new Date(getISODateStringFromMonthYear(month_year));
    const toISODate = new Date(getISODateStringFromMonthYear(month_year));

    const getDataFromDB = () => {
        toISODate.setUTCMonth(toISODate.getUTCMonth() + 1);
        return getMeasurementsByMonth(farm_id, fromISODate, toISODate);
    };

    (validateId(farm_id) && validateDate(fromISODate) && validateDate(toISODate)) ?
        getDataFromDB()
            .then((data) => {
                res.status(200).json(data);
            })
            .catch(() => {
                res.status(500).send('Internal server error');
            })
        :
        res.status(400).send('Bad request');
};

export const byMetricController = (
    req: Request, res: Response
): void => {
    const { metric } = req.query as { [key: string]: string };
    const { farm_id } = req.query as unknown as { [key: string]: mongoose.Types.ObjectId };

    (validateId(farm_id) && validateString(metric)) ?
        getMeasurementsByMetric(farm_id, metric)
            .then((data) => {
                res.status(200).json(data);
            })
            .catch(() => {
                res.status(500).send('Internal server error');
            })
        :
        res.status(400).send('Bad request');

};

export const byMetricByMonthController = (
    req: Request, res: Response
): void => {
    const { metric } = req.query as { [key: string]: string };
    const { month_year } = req.query as { [key: string]: string };
    const { farm_id } = req.query as unknown as { [key: string]: mongoose.Types.ObjectId };
    const fromISODate = new Date(getISODateStringFromMonthYear(month_year));
    const toISODate = new Date(getISODateStringFromMonthYear(month_year));

    const getDataFromDB = () => {
        toISODate.setUTCMonth(toISODate.getUTCMonth() + 1);
        return getMeasurementsByMetricByMonth(farm_id, metric, fromISODate, toISODate);
    };

    (validateId(farm_id) && validateString(metric) && validateDate(fromISODate) && validateDate(toISODate)) ?
        getDataFromDB()
            .then((data) => {
                res.status(200).json(data);
            })
            .catch(() => {
                res.status(500).send('Internal server error');
            })
        :
        res.status(400).send('Bad request');
};

export const monthlyAverageByMetricBetweenMonthsController = (
    req: Request, res: Response
): void => {
    const { metric } = req.query as { [key: string]: string };
    const { from_month_year } = req.query as { [key: string]: string };
    const { to_month_year } = req.query as { [key: string]: string };
    const { farm_id } = req.query as unknown as { [key: string]: mongoose.Types.ObjectId };
    const fromISODate = new Date(getISODateStringFromMonthYear(from_month_year));
    const toISODate = new Date(getISODateStringFromMonthYear(to_month_year));

    const getDataFromDB = () => {
        toISODate.setUTCMonth(toISODate.getUTCMonth() + 1);
        return getMonthlyAveragesByMetricBetweenMonths(farm_id, metric, fromISODate, toISODate);
    };

    (validateId(farm_id) && validateString(metric) && validateDate(fromISODate) && validateDate(toISODate)) ?
        getDataFromDB()
            .then((data) => {
                res.status(200).json(data);
            })
            .catch(() => {
                res.status(500).send('Internal server error');
            })
        :
        res.status(400).send('Bad request');
};

export const minMaxByMetricBetweenMonthsController = (
    req: Request, res: Response
): void => {
    const { metric } = req.query as { [key: string]: string };
    const { from_month_year } = req.query as { [key: string]: string };
    const { to_month_year } = req.query as { [key: string]: string };
    const { farm_id } = req.query as unknown as { [key: string]: mongoose.Types.ObjectId };
    const fromISODate = new Date(getISODateStringFromMonthYear(from_month_year));
    const toISODate = new Date(getISODateStringFromMonthYear(to_month_year));

    const getDataFromDB = () => {
        toISODate.setUTCMonth(toISODate.getUTCMonth() + 1);
        return getMinMaxMeasurementsByMetricBetweenMonths(farm_id, metric, fromISODate, toISODate);
    };

    (validateId(farm_id) && validateString(metric) && validateDate(fromISODate) && validateDate(toISODate)) ?
        getDataFromDB()
            .then((data) => {
                res.status(200).json(data);
            })
            .catch(() => {
                res.status(500).send('Internal server error');
            })
        :
        res.status(400).send('Bad request');
};
