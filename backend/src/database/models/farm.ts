import mongoose from 'mongoose';

const farmSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    measurementIds: {
        type: [mongoose.Types.ObjectId]
    }
});

const Farm = mongoose.model('Farm', farmSchema);

export default Farm;