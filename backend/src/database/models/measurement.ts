import mongoose from 'mongoose';

const measurementSchema = new mongoose.Schema({
    sensorType: {
        type: String,
        required: true
    },
    datetime: {
        type: Date,
        required: true
    },
    value: {
        type: Number,
        required: true
    },
    farmId: {
        type: mongoose.Types.ObjectId,
        required: true
    },
});

const Farm = mongoose.model('Measurement', measurementSchema);

export default Farm;