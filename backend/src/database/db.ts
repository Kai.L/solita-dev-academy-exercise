import mongoose from 'mongoose';
import env from 'dotenv';
env.config();
const MONGODB = process.env.MONGODB_URI as string;

export const connectToDatabase = async () => {
    await mongoose.connect(MONGODB)
        .then(() => {
            console.log('Connected to MongoDB!');
        })
        .catch((error) => {
            console.log('Error connecting to MongDB: ', error.message);
        });
};

export const closeConnectionToDatabase = async () => {
    await mongoose.connection.close()
        .then(() => {
            console.log('Disconnected from MongoDB!');
        })
        .catch((error) => {
            console.log('Error connecting to MongDB: ', error.message);
        });
};