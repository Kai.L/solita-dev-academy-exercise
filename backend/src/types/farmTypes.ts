import mongoose from "mongoose";

export type MeasurementType = {
    sensorType: string,
    datetime: Date,
    value: number,
    farmId: mongoose.Types.ObjectId,
    _id: mongoose.Types.ObjectId,
};

export type FarmType = {
    _id: mongoose.Types.ObjectId,
    name: string,
    measurementIds: Array<mongoose.Types.ObjectId>
};

export type MonthAverageType = {
    value: number,
    datetime: string
};

