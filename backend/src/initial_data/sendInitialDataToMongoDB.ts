
import env from 'dotenv';
env.config();
import { connectToDatabase, closeConnectionToDatabase } from '../database/db';

import * as fs from 'fs';
import Farm from '../database/models/farm';
import Measurement from '../database/models/measurement';
import { validateString, validateMeasurement } from '../lib/utils/validation';
import mongoose, { HydratedDocument } from 'mongoose';

type MeasurementFromCSV = {
    datetime: Date,
    location: string,
    sensorType: string,
    value: number,
};

type InitialFarmType = {
    name: string,
    measurementIds: [mongoose.Types.ObjectId]
};

type InitialMeasurementType = {
    sensorType: string,
    datetime: Date,
    value: number,
    farmId: mongoose.Types.ObjectId
};


const convertCSVToArrayOfObjects = (csvFile: fs.PathLike): Array<MeasurementFromCSV> => {
    const csv = fs.readFileSync(csvFile);
    const lines = csv.toString().split('\n');
    const headers = lines[0].split(',');
    const data = [];

    for (let i = 1; i < lines.length; i++) {
        const obj: { [key: string]: string | Date | number } = {};
        for (let i2 = 0; i2 < headers.length; i2++) {
            const header: string = headers[i2].replace('\r', '');
            let value: string = lines[i].split(',')[i2];
            if (value?.includes('\r')) {
                value = value.replace('\r', '');
            }
            obj[header] = value;
        }
        const objCopy = obj as unknown as MeasurementFromCSV;

        data.push(objCopy);
    }
    return data;
};

const filterValidData = (data: Array<MeasurementFromCSV>) => {
    const validatedData = data.filter((value) => {
        if (
            validateString(value.location)
            && validateMeasurement(value.sensorType, value.datetime, Number(value.value))
        ) {
            return true;
        }
        return false;
    });
    return validatedData;
};

const insertToMongoDB = async (data: Array<MeasurementFromCSV>) => {

    const farm: HydratedDocument<InitialFarmType> = await new Farm({ name: data[0].location, measurementIds: [] }) as HydratedDocument<InitialFarmType>;
    const farmId: mongoose.Types.ObjectId = farm._id as mongoose.Types.ObjectId;

    const validData = filterValidData(data);

    try {
        const measurements: Array<InitialMeasurementType> = [];
        for (let i = 0; i < validData.length; i++) {
            const measurement = new Measurement({
                sensorType: validData[i].sensorType,
                datetime: validData[i].datetime,
                value: validData[i].value,
                farmId: farmId
            }) as HydratedDocument<InitialMeasurementType>;
            measurements.push(measurement);
            const measurementId = measurement._id as mongoose.Types.ObjectId;
            farm.measurementIds.push(measurementId);
        }
        await Measurement.insertMany(measurements);
        await farm.save();
        console.log('Successfully added initial data to DB.');
    } catch (e) {
        console.log(e);
        console.log('Error adding initial data to DB');
    }
};

export const sendInitialDataToMongoDB = () => {
    connectToDatabase()
        .then(async () => {
            const ossi_farm_JSON = convertCSVToArrayOfObjects('./src/initial_data/ossi_farm.csv');
            await insertToMongoDB(ossi_farm_JSON);
            const friman_metsola_JSON = convertCSVToArrayOfObjects('./src/initial_data/friman_metsola.csv');
            await insertToMongoDB(friman_metsola_JSON);
            const Nooras_farm_JSON = convertCSVToArrayOfObjects('./src/initial_data/Nooras_farm.csv');
            await insertToMongoDB(Nooras_farm_JSON);
            const PartialTech_JSON = convertCSVToArrayOfObjects('./src/initial_data/PartialTech.csv');
            await insertToMongoDB(PartialTech_JSON);
        })
        .then(async () => {
            await closeConnectionToDatabase();
        })
        .catch((e) => {
            console.log(e);
        });
};

sendInitialDataToMongoDB();