import './App.css';
import StatisticsPage from './pages/StatisticsPage/StatisticsPage';
import React from 'react';

const App = () => {

	return (
		<div className="App">
			<StatisticsPage />
		</div>
	);
};

export default App;
