import React, {
	createContext, useState, useMemo,
} from 'react';

type MeasurementType = {
    sensorType: string,
    datetime: string,
    value: string,
    farmId: string,
    _id: string,
}

type MeasurementContextType = {
    measurementsState: MeasurementType[],
    setMeasurementsState: (value: MeasurementType[]) => void
};

export const MeasurementsContext = createContext<MeasurementContextType>({
	measurementsState: [],
	setMeasurementsState: () => [],
});

export const MeasurementsProvider: React.FC<React.ReactNode> = ({ children }) => {
	const [measurementsState, setMeasurementsState] = useState<MeasurementType[]>([]);

	const valuesToProvide = useMemo(
		() => ({
			measurementsState,
			setMeasurementsState,
		}),
		[measurementsState],
	);

	return (
		<MeasurementsContext.Provider value={valuesToProvide}>
			{children}
		</MeasurementsContext.Provider>
	);
};
