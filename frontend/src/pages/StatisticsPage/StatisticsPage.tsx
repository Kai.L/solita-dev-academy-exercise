import './StatisticsPage.css';
import React from 'react';
import Search from './Components/Search/Search';
import { MeasurementsProvider } from '../../Contexts/MeasurementsContext';
import GraphAndTable from './Components/GraphAndTable/GraphAndTable';

const StatisticsPage = () => {

	return (
		<div>
			<MeasurementsProvider>
				<GraphAndTable />
				<Search />
			</MeasurementsProvider>
		</div>
	);
};

export default StatisticsPage;