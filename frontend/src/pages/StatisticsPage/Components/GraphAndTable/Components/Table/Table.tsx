import React, { useContext } from 'react';
import './Table.css';
import { MeasurementsContext } from '../../../../../../Contexts/MeasurementsContext';
import { v4 as uuidv4 } from 'uuid';


const Table = () => {
	const { measurementsState } = useContext(MeasurementsContext);

	const getTableBody = () => {
		const getTableBodyRows = () => {
			return measurementsState?.map((measurement) => {
				return (
					<tr key={uuidv4()}>
						<td className='tableRow_Table'>{measurement.datetime}</td>
						<td className='tableRow_Table'>{measurement.sensorType}</td>
						<td className='tableRow_Table'>{measurement.value}</td>
					</tr>
				);
			});
		};

		return (
			<tbody>
				{getTableBodyRows()}
			</tbody>
		);
	};

	return (
		<div className='Table'>
			<table className='table_Table'>
				<thead>
					<tr>
						<th className='tableRow_Table'>Date</th>
						<th className='tableRow_Table'>Metric</th>
						<th className='tableRow_Table'>Value</th>
					</tr>
				</thead>
				{getTableBody()}
			</table>
		</div>
	);
};

export default Table;

