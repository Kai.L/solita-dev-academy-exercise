import React, { useContext, useState } from 'react';
import { MeasurementsContext } from '../../../../../../Contexts/MeasurementsContext';
import {
	Chart as ChartJS,
	CategoryScale,
	LinearScale,
	PointElement,
	LineElement,
	Title,
	Tooltip,
	Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import './Graph.css';

ChartJS.register(
	CategoryScale,
	LinearScale,
	PointElement,
	LineElement,
	Title,
	Tooltip,
	Legend
);
const options = {
	responsive: true,
	maintainAspectRatio: false,
	plugins: {
		legend: {
			position: 'top' as const,
		},
		title: {
			display: true,
			text: 'Measurements',
		},
	},
};

// didnt have time to finish this
const Graph = () => {
	const { measurementsState } = useContext(MeasurementsContext);
	
	const data = {
		labels: measurementsState.map((m) => {
			const date = new Date(m.datetime);
			if (!isNaN(date.getUTCDate())) {
				return `${date.getUTCDate()}.${date.getUTCMonth() + 1}.${date.getUTCFullYear()} | ${date.getUTCHours().toString().length < 2 ?
					`0${date.getUTCHours()}` :
					date.getUTCHours()}:${date.getUTCMinutes().toString().length < 2 ?
					`0${date.getUTCMinutes()}` :
					date.getUTCMinutes()}`;
			}
			return m.datetime;
		}),
		datasets: [
			{
				label: 'Temperature',
				data: measurementsState.map((m) => {
					return (m.sensorType === 'temperature') ?
						m.value : null;
				}),
				borderColor: 'rgb(252, 190, 3)',
				backgroundColor: 'rgba(252, 227, 3, 0.5)',
			},
			{
				label: 'PH',
				data: measurementsState.map((m) => {
					return (m.sensorType === 'pH') ?
						m.value : null;
				}),
				borderColor: 'rgb(18, 227, 43)',
				backgroundColor: 'rgba(18, 227, 43, 0.5)',
			},
			{
				label: 'Rainfall',
				data: measurementsState.map((m) => {
					return (m.sensorType === 'rainFall') ?
						m.value : null;
				}),
				borderColor: 'rgb(20, 103, 219)',
				backgroundColor: 'rgba(20, 103, 219, 0.5)',
			},
			{
				label: 'Average',
				data: measurementsState.map((m) => {
					return !(m.sensorType) ?
						m.value : null;
				}),
				borderColor: 'rgb(219, 31, 78)',
				backgroundColor: 'rgba(219, 31, 78, 0.5)',
			},
		],
	};

	return (
		<div className='Graph'>
			<Line
				height={'500px'}
				data={data}
				options={options}
			/>
		</div>
	);
};

export default Graph;