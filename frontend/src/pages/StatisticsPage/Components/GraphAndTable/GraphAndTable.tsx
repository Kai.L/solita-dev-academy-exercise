import React, { useState } from 'react';
import './GraphAndTable.css';
import Graph from './Components/Graph/Graph';
import Table from './Components/Table/Table';

const GraphAndTable = () => {
	const [selected, setSelected] = useState(1);

	return (
		<div>
			<div className='selectionButtonsWrapper_GraphAndTable'>
				<button
					className='selectionButton_GraphAndTable'
					onClick={() => { setSelected(1); }}
				>
                    Graph
				</button>
				<button
					className='selectionButton_GraphAndTable'
					onClick={() => { setSelected(2); }}
				>
                    Table
				</button>
			</div>
			{selected === 1 ?
				<Graph />
				:
				<Table />
			}
		</div>
	);
};

export default GraphAndTable;