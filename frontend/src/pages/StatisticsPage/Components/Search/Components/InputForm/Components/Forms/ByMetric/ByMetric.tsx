import React, { useEffect, useContext, useState } from 'react';
import DropdownMenu from '../../Input/DropdownMenu/DropDownMenu';
import axios from 'axios';
import { MeasurementsContext } from '../../../../../../../../../Contexts/MeasurementsContext';
import './ByMetric.css';

const ByMetric = () => {
	const [farms, setFarms] = useState([]);
	const [selectedFarm, setSelectedFarm] = useState({ name: '', _id: '' });
	const { setMeasurementsState } = useContext(MeasurementsContext);
	const [selectedMetric, setSelectedMetric] = useState({ name: '', _id: '' });
	const metrics = [
		{ name: 'temperature', _id: '1' },
		{ name: 'pH', _id: '2' },
		{ name: 'rainFall', _id: '3' }
	];

	useEffect(() => {
		axios.get('http://localhost:3001/farms')
			.then((response) => {
				setFarms(response.data);
			}).catch((error) => {
				console.log(error);
			});
	}, []);

	const getData = async () => {
		const response = await axios.get(`http://localhost:3001/farms/measurements/by_metric?farm_id=${selectedFarm._id}&metric=${selectedMetric.name}`);
		setMeasurementsState(response.data);
	};

	return (
		<div className='ByMetric'>
			<p className='header_ByMetric'>BY METRIC</p>
			<div className='formWrapper_ByMetric'>
				<div className='inputsWrapper_ByMetric'>
					<div className='farmsMenu_ByMetric'>
						<DropdownMenu
							selections={farms}
							selected={selectedFarm}
							setSelected={setSelectedFarm}
						/>
					</div>
					<DropdownMenu
						selections={metrics}
						selected={selectedMetric}
						setSelected={setSelectedMetric}
					/>
				</div>
				<button
					className='searchButton_ByMetric'
					onClick={() => { getData(); }}
				>
					Search
				</button>
			</div>
		</div>
	);
};

export default ByMetric;