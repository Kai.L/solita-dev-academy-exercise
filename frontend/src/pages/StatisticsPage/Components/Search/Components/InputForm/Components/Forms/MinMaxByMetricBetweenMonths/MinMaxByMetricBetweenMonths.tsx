import React, { useEffect, useContext, useState } from 'react';
import MonthYearInput from '../../Input/MonthYearInput/MonthYearInput';
import DropdownMenu from '../../Input/DropdownMenu/DropDownMenu';
import axios from 'axios';
import { MeasurementsContext } from '../../../../../../../../../Contexts/MeasurementsContext';
import './MinMaxByMetricBetweenMonths.css';

const MinMaxByMetricBetweenMonths = () => {
	const [farms, setFarms] = useState([]);
	const [fromMonth, setFromMonth] = useState('');
	const [fromYear, setFromYear] = useState('');
	const [toMonth, setToMonth] = useState('');
	const [toYear, setToYear] = useState('');
	const [selectedFarm, setSelectedFarm] = useState({ name: '', _id: '' });
	const [selectedMetric, setSelectedMetric] = useState({ name: '', _id: '' });
	const metrics = [
		{ name: 'temperature', _id: '1' },
		{ name: 'pH', _id: '2' },
		{ name: 'rainFall', _id: '3' }
	];
	const { setMeasurementsState } = useContext(MeasurementsContext);

	useEffect(() => {
		axios.get('http://localhost:3001/farms')
			.then((response) => {
				setFarms(response.data);
			}).catch((error) => {
				console.log(error);
			});
	}, []);

	const getData = async () => {
		const response = await axios.get(`http://localhost:3001/farms/measurements/min_max_by_metric_between_months?farm_id=${selectedFarm._id}&metric=${selectedMetric.name}&from_month_year=${fromMonth}.${fromYear}&to_month_year=${toMonth}.${toYear}`);
		setMeasurementsState(response.data);
	};

	return (
		<div className='MinMaxByMetricBetweenMonths'>
			<p className='header_MinMaxByMetricBetweenMonths'>MIN/MAX BY METRIC BETWEEN MONTHS</p>
			<div className='formWrapper_MinMaxByMetricBetweenMonths'>
				<div className='inputsWrapper_MinMaxByMetricBetweenMonths'>
					<div className='farmsMenu_MinMaxByMetricBetweenMonths'>
						<DropdownMenu
							selections={farms}
							selected={selectedFarm}
							setSelected={setSelectedFarm}
						/>
					</div>
					<DropdownMenu
						selections={metrics}
						selected={selectedMetric}
						setSelected={setSelectedMetric}
					/>
					<div className='monthYearInput_MinMaxByMetricBetweenMonths'>
						<p
							className='monthYearInputLabel_MinMaxByMetricBetweenMonths'
						>
                            From:
						</p>
						<MonthYearInput
							month={fromMonth}
							setMonth={setFromMonth}
							year={fromYear}
							setYear={setFromYear}
						/>
					</div>
					<div className='monthYearInput_MinMaxByMetricBetweenMonths'>
						<p
							className='monthYearInputLabel_MinMaxByMetricBetweenMonths'
						>
                            To:
						</p>
						<MonthYearInput
							month={toMonth}
							setMonth={setToMonth}
							year={toYear}
							setYear={setToYear}
						/>
					</div>
				</div>
				<button
					className='searchButton_MinMaxByMetricBetweenMonths'
					onClick={() => { getData(); }}
				>
                    Search
				</button>
			</div>
		</div>
	);
};

export default MinMaxByMetricBetweenMonths;