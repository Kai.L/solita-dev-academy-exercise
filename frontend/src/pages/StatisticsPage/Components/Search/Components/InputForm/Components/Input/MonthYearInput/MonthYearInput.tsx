import React from 'react';
import './MonthYearInput.css';

interface Props {
	month: string,
	setMonth: React.Dispatch<React.SetStateAction<string>>
	year: string,
	setYear: React.Dispatch<React.SetStateAction<string>>
}

const MonthYearInput = ({ month, setMonth, year, setYear }: Props) => {

	return (
		<div className='MonthYearInput'>
			<div className='inputsWrapper_MonthYearInput'>
				<div className='monthInputWrapper_MonthYearInput'>
					<p className='inputLabel_MonthYearInput'>Month</p>
					<input
						className='monthInput_MonthYearInput'
						placeholder='MM'
						value={month}
						onChange={(event) => { setMonth(event.target.value); }}
					/>
				</div>
				<div className='inputWrapper_MonthYearInput'>
					<p className='inputLabel_MonthYearInput'>Year</p>
					<input
						className='yearInput_MonthYearInput'
						placeholder='YYYY'
						value={year}
						onChange={(event) => { setYear(event.target.value); }}
					/>
				</div>
			</div>
		</div>
	);
};

export default MonthYearInput;