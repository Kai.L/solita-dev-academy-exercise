import React, { useState } from 'react';
import './DropdownMenu.css';
import { v4 as uuidv4 } from 'uuid';

interface Props {
	selections: Array<{ name: string, _id: string }>
	selected: { name: string, _id: string },
	setSelected: React.Dispatch<React.SetStateAction<{ name: string, _id: string }>>
}

const DropdownMenu = ({ selections, selected, setSelected }: Props) => {

	const [isMenuOpened, setIsMenuOpened] = useState(false);


	const getSelectionsMenu = (selections: Array<{ name: string, _id: string }>) => {
		const selectionsMenu = selections.map((selection, i) => {
			if (i % 2 === 0) {
				return <button
					key={uuidv4()}
					className='selection_DropdownMenu'
					onClick={() => {
						setSelected(selection);
						setIsMenuOpened(false);
					}}
				>
					{selection.name}
				</button>;
			}
			return <button
				key={uuidv4()}
				className='selection2_DropdownMenu'
				onClick={() => {
					setSelected(selection);
					setIsMenuOpened(false);
				}}
			>
				{selection.name}
			</button>;
		});
		return <div className='selectionsWrapper_DropdownMenu'>
			{selectionsMenu}
		</div>;
	};

	return (
		<div className='DropdownMenu'>
			<button
				className='openMenuButton_DropdownMenu'
				onClick={() => {
					setIsMenuOpened(!isMenuOpened);
				}}
			>
				{selected.name ?
					selected.name
					:
					'Select'
				}
			</button>
			{isMenuOpened ?
				getSelectionsMenu(selections)
				:
				null
			}
		</div>
	);
};

export default DropdownMenu;