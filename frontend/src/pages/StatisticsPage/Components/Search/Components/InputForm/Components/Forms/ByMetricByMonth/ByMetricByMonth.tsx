import React, { useEffect, useContext, useState } from 'react';
import MonthYearInput from '../../Input/MonthYearInput/MonthYearInput';
import DropdownMenu from '../../Input/DropdownMenu/DropDownMenu';
import axios from 'axios';
import { MeasurementsContext } from '../../../../../../../../../Contexts/MeasurementsContext';
import './ByMetricByMonth.css';

const ByMetricByMonth = () => {
	const [farms, setFarms] = useState([]);
	const [month, setMonth] = useState('');
	const [year, setYear] = useState('');
	const [selectedFarm, setSelectedFarm] = useState({ name: '', _id: '' });
	const [selectedMetric, setSelectedMetric] = useState({ name: '', _id: '' });
	const metrics = [
		{ name: 'temperature', _id: '1' },
		{ name: 'pH', _id: '2' },
		{ name: 'rainFall', _id: '3' }
	];
	const { setMeasurementsState } = useContext(MeasurementsContext);

	useEffect(() => {
		axios.get('http://localhost:3001/farms')
			.then((response) => {
				setFarms(response.data);
			}).catch((error) => {
				console.log(error);
			});
	}, []);

	const getData = async () => {
		const response = await axios.get(`http://localhost:3001/farms/measurements/by_metric_by_month?farm_id=${selectedFarm._id}&metric=${selectedMetric.name}&month_year=${month}.${year}`);
		setMeasurementsState(response.data);
	};

	return (
		<div className='ByMetricByMonth'>
			<p className='header_ByMetricByMonth'>BY METRIC BY MONTH</p>
			<div className='formWrapper_ByMetricByMonth'>
				<div className='inputsWrapper_ByMetricByMonth'>
					<div className='farmsMenu_ByMetricByMonth'>
						<DropdownMenu
							selections={farms}
							selected={selectedFarm}
							setSelected={setSelectedFarm}
						/>
					</div>
					<DropdownMenu
						selections={metrics}
						selected={selectedMetric}
						setSelected={setSelectedMetric}
					/>
					<div className='monthYearInput_ByMetricByMonth'>
						<MonthYearInput
							month={month}
							setMonth={setMonth}
							year={year}
							setYear={setYear}
						/>
					</div>
				</div>
				<button
					className='searchButton_ByMetricByMonth'
					onClick={() => { getData(); }}
				>
                    Search
				</button>
			</div>
		</div>
	);
};

export default ByMetricByMonth;