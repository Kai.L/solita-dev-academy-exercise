import React, { useState } from 'react';
import OpenInputFormButtons from './Components/OpenInputFormButtons/OpenInputFormButtons';
import './InputForm.css';
import ByMonth from './Components/Forms/ByMonth/ByMonth';
import ByMetric from './Components/Forms/ByMetric/ByMetric';
import ByMetricByMonth from './Components/Forms/ByMetricByMonth/ByMetricByMonth';
import MonthlyAverageByMetricBetweenMonths from './Components/Forms/MonthlyAverageByMetricBetweenMonths/MonthlyAverageByMetricBetweenMonths';
import MinMaxByMetricBetweenMonths from './Components/Forms/MinMaxByMetricBetweenMonths/MinMaxByMetricBetweenMonths';


const InputForm = () => {
	const [chosenInputForm, setChosenInputForm] = useState(0);

	return (
		<div className='InputForm'>
			<OpenInputFormButtons
				setChosenInputForm={setChosenInputForm}
			/>
			{chosenInputForm === 1 ?
				<ByMonth />
				:
				chosenInputForm === 2 ?
					<ByMetric />
					:
					chosenInputForm === 3 ?
						<ByMetricByMonth />
						:
						chosenInputForm === 4 ?
							<MonthlyAverageByMetricBetweenMonths />
							:
							chosenInputForm === 5 ?
								<MinMaxByMetricBetweenMonths />
								:
								null
			}
		</div>
	);
};

export default InputForm;