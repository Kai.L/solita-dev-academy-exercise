import React, { useEffect, useContext, useState } from 'react';
import MonthYearInput from '../../Input/MonthYearInput/MonthYearInput';
import DropdownMenu from '../../Input/DropdownMenu/DropDownMenu';
import axios from 'axios';
import { MeasurementsContext } from '../../../../../../../../../Contexts/MeasurementsContext';
import './ByMonth.css';

const ByMonth = () => {
	const [farms, setFarms] = useState([]);
	const [month, setMonth] = useState('');
	const [year, setYear] = useState('');
	const [selectedFarm, setSelectedFarm] = useState({ name: '', _id: '' });
	const { setMeasurementsState } = useContext(MeasurementsContext);

	useEffect(() => {
		axios.get('http://localhost:3001/farms')
			.then((response) => {
				setFarms(response.data);
			}).catch((error) => {
				console.log(error);
			});
	}, []);

	const getData = async () => {
		const response = await axios.get(`http://localhost:3001/farms/measurements/by_month?farm_id=${selectedFarm._id}&month_year=${month}.${year}`);
		setMeasurementsState(response.data);
	};

	return (
		<div className='ByMonth'>
			<p className='header_ByMonth'>BY MONTH</p>
			<div className='formWrapper_ByMonth'>
				<div className='inputsWrapper_ByMonth'>
					<DropdownMenu
						selections={farms}
						selected={selectedFarm}
						setSelected={setSelectedFarm}
					/>
					<div className='monthYearInput_ByMonth'>
						<MonthYearInput
							month={month}
							setMonth={setMonth}
							year={year}
							setYear={setYear}
						/>
					</div>
				</div>
				<button
					className='searchButton_ByMonth'
					onClick={() => { getData(); }}
				>
					Search
				</button>
			</div>
		</div>
	);
};

export default ByMonth;