import React from 'react';
import './OpenInputFormButtons.css';

interface Props {
	setChosenInputForm: React.Dispatch<React.SetStateAction<number>>
}

const OpenInputFormButtons = ({ setChosenInputForm }: Props) => {


	return (
		<div className='OpenInputFormButtons'>
			<div className='buttonsWrapper_OpenInputFormButtons'>
				<button
					className='button_OpenInputFormButtons'
					onClick={() => { setChosenInputForm(1); }}
				>
					By month
				</button>
				<button
					className='button_OpenInputFormButtons'
					onClick={() => { setChosenInputForm(2); }}
				>
					By metric
				</button>
				<button
					className='button_OpenInputFormButtons'
					onClick={() => { setChosenInputForm(3); }}
				>
					By metric by month
				</button>
				<button
					className='button_OpenInputFormButtons'
					onClick={() => { setChosenInputForm(4); }}
				>
					Monthly average by metric between months
				</button>
				<button
					className='button_OpenInputFormButtons'
					onClick={() => { setChosenInputForm(5); }}
				>
					Min/Max by metric between months
				</button>
			</div>
		</div>
	);
};
export default OpenInputFormButtons;