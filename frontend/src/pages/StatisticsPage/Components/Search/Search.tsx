import React from 'react';
import InputForm from './Components/InputForm/InputForm';
import './Search.css';

const Search = () => {

	return (
		<div className='Search'>
			<InputForm />
		</div>
	);
};

export default Search;