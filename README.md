
# Solita Dev Academy pre-assignment

Application for showing measurements (PH/Temperature/Rainfall) from the following farms:

* Noora's farm
* Friman Metsola collective
* Organic Ossi's Impact That Lasts plantation 
* PartialTech Research Farm

## How to run

(I used windows 11 for this and I am not sure if there might be some compability issues when running this with other operating systems.)

* Step 1: If you don't have Node.js installed then install the latest version from here --> https://nodejs.org/en/download/
(I used v16.13.0)
* Step 2: Go to the Backend directory in your command line (example: C:\Users\user\Desktop\dev-academy-2022-exercise\backend) and run command "npm install"
* Step 3: Create an .env file in the backend root directory and add the next 2 lines:
MONGODB_URI=You will receive this part from me.
PORT=3001
* Step 4: Run command "npm run ts-node src/initial_data/sendInitialDataToMongoDB"
* Step 5: Run command "npm run dev"
* Step 6: Go to the Frontend directory in your other command line (example: C:\Users\user\Desktop\dev-academy-2022-exercise\frontend) and run command "npm install"
* Step 7: Run command "npm start"

Now the backend should be running ("http://localhost:3001") with a connection to the database and you can see the frontend running ("http://localhost:3000") in your browser,
if the frontend didn't open automatically after couple of seconds then type "localhost:3000" in your browser's address bar and press enter.

## Endpoints:

### GET /farms
All farms (name, _id)

### GET /farms/measurements/by_month?farm_id=61d4037e67d9f0d90ff12d21&month_year=9.2020
All measurements from farm by month (_id, value, sensorType, datetime)

### GET /farms/measurements/by_metric?farm_id=61d4037e67d9f0d90ff12d21&metric=rainFall
All measurements from farm by metric (_id, value, sensorType, datetime)

### GET /farms/measurements/by_metric_by_month?farm_id=61d4037e67d9f0d90ff12d21&metric=rainFall&month_year=9.2020
All measurements from farm by metric and by month (_id, value, sensorType, datetime)

### GET /farms/measurements/min_max_by_metric_between_months?farm_id=61d4037e67d9f0d90ff12d21&metric=rainFall&from_month_year=9.2020&to_month_year=10.2020
Lowest and highest measurement by metric and between months (_id, value, sensorType, datetime)

### GET /farms/measurements/monthly_avg_by_metric_between_months?farm_id=61d4037e67d9f0d90ff12d21&metric=rainFall&from_month_year=9.2020&to_month_year=9.2021
Monthly average by measurement and between months (value, datetime)

## Run tests

Go to backend root directory in your command line and run command "npm run test"

## Technologies used

### Frontend:

* React
* Typescript
* axios
* chart.js
* react-chartjs-2
* uuid
* eslint
* eslint-plugin-react

### Backend:

* typescript
* node.js
* express
* mongodb
* express-mongo-sanitize
* cors
* dotenv
* ts-node
* ts-jest
* ts-node-dev
* jest
* supertest
* eslint